<?php 
    require 'includes/app.php';
    incluirTemplate('header');
?>

    <main class="contenedor seccion">
        <h1>Conoce sobre Nosotros</h1>

        <div class="contenido-nosotros">
            <div class="imagen">
                <picture>
                    <source srcset="build/img/nosotros.webp" type="image/webp">
                    <source srcset="build/img/nosotros.jpg" type="image/jpeg">
                    <img loading="lazy" src="build/img/nosotros.jpg" alt="Sobre Nosotros">
                </picture>
            </div>

            <div class="texto-nosotros">
                <blockquote>
                    25 Años de experiencia
                </blockquote>

                <p>Con más de 25 años de experiencia en el mercado inmobiliario, en Bienes y Raices nos enorgullece ofrecerte un nivel incomparable de conocimiento y profesionalismo.

Durante más de dos décadas, hemos estado ayudando a nuestros clientes a alcanzar sus objetivos inmobiliarios con éxito. Nuestra vasta experiencia nos ha permitido desarrollar una comprensión profunda del mercado local y dominar las complejidades del proceso de compra, venta o alquiler de propiedades.

Lo que nos distingue es nuestro compromiso inquebrantable con la excelencia en el servicio al cliente. Con cada transacción, nos esforzamos por superar las expectativas, brindando orientación experta, atención personalizada y soluciones creativas a nuestros clientes.

Nuestro historial comprobado de éxito y nuestra larga lista de clientes satisfechos son testimonio de nuestra dedicación y profesionalismo en el campo de los bienes raíces. Confía en nuestra experiencia para guiarte en cada paso del camino y llevar tus objetivos inmobiliarios al siguiente nivel.

Ya sea que estés buscando comprar, vender o alquilar una propiedad, en Bienes y raices estamos aquí para ayudarte a hacer realidad tus sueños inmobiliarios. Contáctanos hoy mismo y descubre cómo nuestra experiencia puede marcar la diferencia para ti.</p>            </div>
        </div>
    </main>

    <section class="contenedor seccion">
        <h1>Más Sobre Nosotros</h1>

        <div class="iconos-nosotros">
            <div class="icono">
                <img src="build/img/icono1.svg" alt="Icono seguridad" loading="lazy">
                <h3>Seguridad</h3>
                <p>Muchas propiedades residenciales y comerciales utilizan sistemas de acceso controlado, como puertas con cerraduras electrónicas, tarjetas de acceso o códigos de entrada, para regular quién puede ingresar a la propiedad.</p>
            </div>
            <div class="icono">
                <img src="build/img/icono2.svg" alt="Icono Precio" loading="lazy">
                <h3>Precio</h3>
                <p>Por tiempo limitado, estamos ofreciendo un descuento del 10% en todas las comisiones de agencia para nuestros clientes nuevos y existentes. Ya sea que estés buscando comprar, vender o alquilar, nuestro experimentado equipo de agentes está aquí para ayudarte en cada paso del camino.</p>
            </div>
            <div class="icono">
                <img src="build/img/icono3.svg" alt="Icono Tiempo" loading="lazy">
                <h3>A Tiempo</h3>
                <p>No pierdas más tiempo buscando en sitios interminables o tratando con agentes poco profesionales. Déjanos hacer el trabajo por ti y encuentra tu hogar ideal de manera rápida y sin estrés.</p>
            </div>
        </div>
    </section>

<?php 
    incluirTemplate('footer');
?>