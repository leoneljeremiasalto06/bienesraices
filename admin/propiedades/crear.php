<?php

use App\Propiedad;

require '../../includes/app.php';

use Intervention\Image\ImageManagerStatic as Image;

$titulo = '';
$precio = '';
$descripcion = '';
$habitaciones = '';
$wc = '';
$estacionamiento = '';
$vendedorId = '';
$errores_propiedad = [];

$auth = estaAutenticado();

if (!$auth) {
    header('Location: /');
}

$db = conectarDB();

// Consultar para obtener los vendedores
$consulta = "SELECT * FROM vendedores";




$resultado = mysqli_query($db, $consulta);


// Ejecutar el código después de que el usuario envia el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // Asignar files hacia una variable
    $imagen = $_FILES['imagen'];
    $caracteristicas_propiedad = $_POST + $imagen;
    $propiedad_nueva = new Propiedad($caracteristicas_propiedad);
    $errores_propiedad = $propiedad_nueva->get_errores();

    // Subida de archivos
    $carpetaImagenes = '../../imagenes/';

    //Si no esta la carpeta de imagenes creala
    if (!is_dir($carpetaImagenes)) {
        mkdir($carpetaImagenes, 0755, true);
    }
    if (empty($errores_propiedad)) {
        // Generar un nombre único
        $nombreImagen = md5(uniqid(rand(), true)) . ".jpg";

        // Realizar resize con Intervention Image
        $img = Image::make($_FILES['imagen']['tmp_name'])->fit(800, 600); // Ajusta el tamaño según tus necesidades

        // Guardame el nombre de la imagen en la instancia nueva de clase propiedad
        $propiedad_nueva->setImagen($nombreImagen);

        // Guardar la imagen redimensionada
        $img->save($carpetaImagenes . $nombreImagen);

        $resultado = $propiedad_nueva->guardar();

        if ($resultado) {
            // Redireccionar al usuario.
            header('Location: /admin?resultado=1');
        }
    }
    else{

    }

}
incluirTemplate('header');
if (!isset($_SESSION)) {
    session_start();
}

$auth = $_SESSION['login'] ?? false;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bienes Raices</title>
    <link rel="stylesheet" href="/build/css/app.css">
</head>

<body>

    <header>
        <div class="contenedor contenido-header">
            <div class="barra">
                <a href="/">
                    <img src="/build/img/logo.svg" alt="Logotipo de Bienes Raices">
                </a>

                <div class="mobile-menu">
                    <img src="/build/img/barras.svg" alt="icono menu responsive">
                </div>


            </div> <!--.barra-->

        </div>
    </header>

    <main class="contenedor seccion">
        <h1>Crear</h1>



        <a href="/" class="boton boton-verde">Volver</a>

        <?php foreach ($errores_propiedad as $error) : ?>
            <div class="alerta error">
                <?php echo $error; ?>
            </div>
        <?php endforeach; ?>

        <form class="formulario" method="POST" action="/admin/propiedades/crear.php" enctype="multipart/form-data">
            <fieldset>
                <legend>Información General</legend>

                <label for="titulo">Titulo:</label>
                <input type="text" id="titulo" name="titulo" placeholder="Titulo Propiedad" value="<?php echo $titulo; ?>">

                <label for="precio">Precio:</label>
                <input type="number" id="precio" name="precio" placeholder="Precio Propiedad" value="<?php echo $precio; ?>">

                <label for="imagen">Imagen:</label>
                <input type="file" id="imagen" accept="image/jpeg, image/png" name="imagen">

                <label for="descripcion">Descripción:</label>
                <textarea id="descripcion" name="descripcion"><?php echo $descripcion; ?></textarea>

            </fieldset>

            <fieldset>
                <legend>Información Propiedad</legend>

                <label for="habitaciones">Habitaciones:</label>
                <input type="number" id="habitaciones" name="habitaciones" placeholder="Ej: 3" min="1" max="9" value="<?php echo $habitaciones; ?>">

                <label for="wc">Baños:</label>
                <input type="number" id="wc" name="wc" placeholder="Ej: 3" min="1" max="9" value="<?php echo $wc; ?>">

                <label for="estacionamiento">Estacionamiento:</label>
                <input type="number" id="estacionamiento" name="estacionamiento" placeholder="Ej: 3" min="1" max="9" value="<?php echo $estacionamiento; ?>">

            </fieldset>

            <fieldset>
                <legend>Vendedor</legend>

                <select name="vendedorId">
                    <option value="">-- Seleccione --</option>
                    <?php while ($vendedorId =  mysqli_fetch_assoc($resultado)) : ?>
                        <option <?php echo $vendedorId === $vendedorId['id'] ? 'selected' : ''; ?> value="<?php echo $vendedorId['id']; ?>"> <?php echo $vendedorId['nombre'] . " " . $vendedorId['apellido']; ?> </option>
                    <?php endwhile; ?>
                </select>
            </fieldset>

            <input type="submit" value="Crear Propiedad" class="boton boton-verde">
        </form>

    </main>
    