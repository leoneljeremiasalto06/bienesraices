<?php

namespace App;

use DateTime;


class Propiedad
{
    //forma que van a tener los datos, con este arrelgo podemos mapear y unir los atributos
    protected static $columnas_db = ['titulo', 'precio', 'name', 'descripcion', 'habitaciones', 'wc', 'estacionamiento', 'creado', 'vendedorId'];
    static protected $db;
    public  $id;
    public  $titulo;
    public  $precio;
    public  $name;
    public  $fullpath;
    public  $imagen_error;
    public  $imagen_size;
    public  $descripcion;
    public  $habitaciones;
    public  $wc;
    public  $estacionamiento;
    public  $creado;
    public  $vendedorId;
    // Validar por tamaño (1mb máximo)
    public $medida = 1000 * 1000;
    //errores
    public static $errores = [];


    //esta funcion va recerbar en $db la conexion de la base de datos
    public static function setDB($dbconexion)
    {
        self::$db = $dbconexion;
    }

    public function __construct(array $args = [])
    {
        $fecha = new DateTime();
        $this->creado = $fecha->format('Y-m-d H:i:s');
        $this->id = $args['id'] ?? 0;
        $this->titulo = $args['titulo'] ?? '';
        $this->precio = $args['precio'] ?? 0.0;
        $this->name = $args['name'] ?? '';
        $this->imagen_error = $args['error'] ?? '';
        $this->imagen_size = $args['size'] ?? '';
        $this->descripcion = $args['descripcion'] ?? '';
        $this->habitaciones = (int)$args['habitaciones'] ?? 0;
        $this->wc = (int)$args['wc'] ?? 0;
        $this->estacionamiento = (int)$args['estacionamiento'] ?? 0;
        $this->vendedorId = $args['vendedorId'] ?? '';
    }

    //1. creamos el motodo de la clase que nos va a permitir guardar a la base de datos la nueva clase.
    public function guardar(): bool
    {
        $atributos_sanitizados = $this->sanitizarAtributos();

        //2. creamos la query
        $query = "INSERT INTO propiedades (titulo, precio, imagen, descripcion, habitaciones, wc, estacionamiento, creado, vendedorId) 
    VALUES ('$atributos_sanitizados[titulo]', '$atributos_sanitizados[precio]', '$atributos_sanitizados[name]', '$atributos_sanitizados[descripcion]', '$atributos_sanitizados[habitaciones]', '$atributos_sanitizados[wc]', '$atributos_sanitizados[estacionamiento]', '$atributos_sanitizados[creado]', '$atributos_sanitizados[vendedorId]')";
        //2.5 sanitizamos los atributos que se ingresaron en el formulario
        //3. realizamos la conexión a la base de datos

        self::$db->query($query);
        return true;
    }

    public function verificarErrores()
    {
    }
    public function mostrar_errores()
    {
    }
    public function atributos()
    {
        $atributos = [];
        foreach (self::$columnas_db as $columna) {
            if ($columna === 'id') continue;
            $atributos[$columna] = $this->$columna;
        }
        return $atributos;
    }
    public function sanitizarAtributos()
    {
        $atributos = $this->atributos();
        $arreglo_sanitizado = [];
        foreach ($atributos as $key => $value) {
            $arreglo_sanitizado[$key] = self::$db->escape_string($value); //sanitizamos el atributo
        }
        return $arreglo_sanitizado;
    }

    public function setImagen($img)
    {
        $this->name = $img;
    }

    public function get_errores(): array
    {
        self::$errores = []; // Limpiar errores anteriores

        // Validaciones

        if (!$this->precio) {
            self::$errores[] = 'El precio es obligatorio';
        }
        if (!$this->name) {
            self::$errores[] = 'El nombre es obligatorio';
        }
        if (!$this->habitaciones) {
            self::$errores[] = 'El Número de habitaciones es obligatorio';
        }

        if (!$this->wc) {
            self::$errores[] = 'El Número de Baños es obligatorio';
        }

        if (!$this->estacionamiento) {
            self::$errores[] = 'El Número de lugares de Estacionamiento es obligatorio';
        }

        if (!$this->vendedorId) {
            self::$errores[] = 'Elige un vendedor';
        }

        if (!$this->name || $this->imagen_error) {
            self::$errores[] = 'La Imagen es Obligatoria';
        }

        if ($this->imagen_size > $this->medida) {
            self::$errores[] = 'La Imagen es muy pesada';
        }


        return self::$errores;
    }


    public function all(){
        //aca debe dar todos los objetos de una consulta de la base de datos
    }
}
